'use strict'

var path = require('path');
var fs = require('fs');


var configLocation = '/etc/onlinefs/setting.conf';

try {
    fs.accessSync(configLocation, fs.R_OK);
} catch (err) {
    configLocation = path.join(__dirname, 'setting.conf');
}

module.exports = () => JSON.parse(fs.readFileSync(configLocation));
