#!/bin/bash
if [ "$1" == "" ]; then
    echo "$0 {tag} [upload]";
    exit 1;
fi

image=gcr.io/online-file-storage/onlinefs-service:$1
if [ "$2" == "upload" ]; then
    echo "upload image version: $1"
    gcloud docker push $image

    echo "finish uploading"
    echo "Launch a pod: kubectl run {pod-name} --image=$image -- [api-service.js|identify-service.js|identify-result-service.js]" 
else
    echo "build image version: $1"
    docker build -f Dockerfiles/Dockerfile -t $image .
    echo "finish building"
fi
