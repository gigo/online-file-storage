# Online File Storage (onlinefs) #

Well, the name of this project is very straightforward (lol). As the name suggests, users could put any files onto cloud storage. And what the name does not say is, for image content only, it will parse the image to find out objects.

You could try it with our backend site:

```
    https://api.onlinefs.gigo.rocks:3000
```

**Since it's https, if you want to test the api, you have to ignore the ssl verification. For `httpie`, use `--verify=no`**

## How to use ##

The follow instructions require the pre-install of `httpie`. Of course, you can use `curl`, too. I use `httpie` because it is better looking for http request/response formatting.

1. Register an user
    
    ``http -f POST api.onlinefs.gigo.rocks:3000/auth/register email=xxx@gmail.com password=hello``

    which will return a **JWT** token. The token looks like:

    ``{ token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.xxxxxxxxxxx.yyyyyyy' }``
    

2. If you have registered before, you have to login first.
	
	``http -f POST api.onlinefs.gigo.rocks:3000/auth/login email=xxx@gmail.com password=hello``
	
	In this way, it also returns a **JWT** token.	 
	
	***Note that the JWT token is for authorization, without it, calling the apis other than register/login will fail.*** 

3. Upload a file
	
	``http -f POST api.onlinefs.gigo.rocks:3000/file 'Authorization: Bearer the-jwt-token' f@myfile.jpg``
	
4. Get file list
	
	``http api.onlinefs.gigo.rocks:3000/file 'Authorization: Bearer the-jwt-token``
	
	And it will return a list of file info.
	
	```
		[
			{
				"_id": "56e1aa6d118f3b2652d9f955",
				"date": 1457629805426,
				"name": "myfile.jpg",
				"tags": ["cat", "animal", "mammal"]
			}
		]
	```
	
	From above, each field reprents:

	Field		| Description
	----------|---------------
	_id			| The file id, which could be used for downloading
	date		| The uploading date in UTC miliseconds
	name		| The display name of the file
	tags      | Possible objects appear in this image
	
5. Download file

	``http localhost:8080/file/56e1aa6d118f3b2652d9f955 'Authorization: Bearer the-jwt-token > downloaded.jpg``
	
	where `56e1aa6d118f3b2652d9f955` is the file id coming from fetching the file list.

 
## Dependency ##
*   mongodb (v2.4.9)
*   node (v5.3.0+)
*   rabbitmq (v3.2.4)

## Prerequisite ##

Please follow the following instructions: (information from [here](https://github.com/GoogleCloudPlatform/gcloud-node))


1. Visit the Google Developers Console.
2. Create a new project or click on an existing project.
3. Navigate to **APIs & auth > APIs** section and turn on the following APIs (you may need to enable billing in order to use these services):
	* Google Cloud Datastore API
	* Google Cloud Storage
	* Google Cloud Storage JSON API
4. Navigate to **APIs & auth > Credentials** and then:
	* If you want to use a new service account, click on Create new Client ID and select Service account. After the account is created, you will be prompted to download the JSON key file that the library uses to authenticate your requests.
	* If you want to generate a new key for an existing service account, click on Generate new JSON key and download the JSON key file.


## Instruction for development ##
1. Setup `mongodb`: in **mac**, you can run it by `mongod -f conf/mongodb.yaml`
2. `npm install` to install project dependencies.
3. create a `setting.conf` file and modify the content. You can reference `setting.conf.example`
4. obtain the google credential (JSON format) from google cloud console and modify its location in `setting.conf`
5. `DEBUG=* node main.js` then it should work if everything goes well.

## Deployment ##

In the following sections, we provide two way to deploy our service. The first one is to setup whole the things by yourself. And a easier way is to use `Docker`.

### All-in-1 deployment by docker ###

Though in docker philosophy, each container executes one major service. However, for quickly start to play, I had setup a all-in-1 Dockerfile. All you have to do is to request a google cloud credential (which enables vision/storage). Anyway, if you are not willing to use google cloud, I suppose you can try register/login functions. Here are the steps to setup a all-in-one docker image.

1.  setup docker environment
2.  modify `setting.conf` and put it along with google cloud credential to `dist/etc`. There will be copied to `/etc/onlinefs` in the docker image. If you want to use HTTPS protocol, you have to put certificate/private key into `dist/etc` and setup the path in `setting.conf`.
3.  in the project root path, type `docker build -f Dockerfiles/Dockerfile.allin1 -t onlinefs-services-allin1:v1 .`
4.  now, we will run the docker in foreground mode (sure, you can run it in background, refer below). `docker run -it -e 'MQ_USER=meow' -e 'MQ_PASS=meow' -P 1111:80 -P 2222:15672 onlinefs-services-allin1:v1 /bin/bash`

    ***Note that the MQ\_USER / MQ\_PASS should confrom to the login/password key in amqp section in setting.conf***

5.  we can try some APIs with the port 1111, and use browser connect to port 15672 for rabbitmq management page.

    ***Note that if you are using mac, we have to know the docker daemon ip first. Type `docker-machine ip` to get the ip, e.g. 192.168.99.100, then `http -f POST 192.168.99.100:1111/...`***


### Deployment by docker ###

This Dockerfiles/Dockerfile is the one which builds the image running on the gcloud. 

1.  setup docker environment
2.  modify `setting.conf` and put it along with google cloud credential to `dist/etc`. There will be copied to `/etc/onlinefs` in the docker image. If you want to use HTTPS protocol, you have to put certificate/private key into `dist/etc` and setup the path in `setting.conf`.
3.  in the project root path, type `./docker-deploy.sh v1` which build a docker image with tag `v1`. once finished building the image, type `docker images` and you will see an image like this: 

    ```
    gcr.io/online-file-storage/onlinefs-service
    ```
    
4.  now, we are going to start docker container in the background to run our services, and here is the command: 

	1.  api-service: `docker run -d -p 9527:3000 gcr.io/online-file-storage/onlinefs-service:v1 api-service.js`
	
	2.  identify-service: `docker run -d -p 9527:3000 gcr.io/online-file-storage/onlinefs-service:v1 identify-service.js`
	
	3.  identify-result-service: `docker run -d -p 9527:3000 gcr.io/online-file-storage/onlinefs-service:v1 identify-result-service.js`
   
    , which in turn would display a container id, said `1234ee....` (we will use it later). 

    Now, we can access local port *9527* for the port *3000* in the container.
    
    ***Note that since we use google container engine to host services, each service is resided in different pod. Once the image is uploaded, we could run it using (well, I admit skip lots of stuffs about gcloud deployment, but basically, I follow the instructions [here] (http://kubernetes.io/docs/hellonode/))*** 
    
    ```
    kubectl run {pod-name} --image=gcr.io/online-file-storage/onlinefs-service:v1 -- [api-service.js|identify-service.js|identify-result-service.js]
    ```
    

5.  you could try the command list below, e.g. to register an user by using the specific host port, such as: `http -f POST localhost:9527/...`

    ***Note that if you are using mac, we have to know the docker daemon ip first. Type `docker-machine ip` to get the ip, e.g. 192.168.99.100, then `http -f POST 192.168.99.100:9527/...`***
   
6.  to check the log, you could type:

    `docker logs -f 1234ee` (the id is from above)
	 

### Maunally deployment ###
1. assume you know how to setup for development (reference above)
2. create a nologin user `onlinefs`
3. copy `conf/linux/upstart/*` to `/etc/init`
4. copy `setting.conf` to `/etc/onlinefs`
5. put the source code to the location described in `onlinefs-api-service.conf`
6. start each service. e.g.: `sudo start onlinefs-api-service`

