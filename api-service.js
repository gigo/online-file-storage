'use strict';

var debug = require('debug')('api-service');
var error = require('debug')('api-service:err');
var koa = require('koa');
var app = koa();
var conf = require('./config.js')();
var fs = require('fs');

// Force HTTPS requests
if (conf.useSSL) {
    app.use(require('koa-sslify')());
}

app.use(require('koa-mongo')({
    host: conf.mongo.host,
    port: conf.mongo.port
}));

app.use(function *(next){
    var start = new Date;
    yield next;
    var ms = new Date - start;
    debug('%s %s - %s', this.method, this.url, ms);
});

var file = require('./routes').file;
var auth = require('./routes').auth;
var router = require('koa-router')();
var koaBody = require('koa-body')();
var jwt = require('koa-jwt');

app.use(jwt({secret: conf.authSecret, key: 'token' }).unless({path: ['/auth/register', '/auth/login']}));

router.get('/file', file.preprocess, file.list);
router.get('/file/:id', file.preprocess, koaBody, file.download);
router.get('/file/path/:id', file.preprocess, koaBody, file.getDownloadLink);
router.post('/file', file.preprocess, koaBody, file.upload);
router.post('/auth/register', koaBody, auth.register);
router.post('/auth/login', koaBody, auth.login);

app.use(router.routes());
app.use(router.allowedMethods());

// setup common response headers
app.use(function *(next) {
    this.set('Access-Control-Allow-Origin', '*');
    this.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    yield next;
});

app.on('error', function(err, ctx){
    error('server error', ctx);
    error(err.stack);
});


if (conf.useSSL) {
    require('https').createServer({
        key: fs.readFileSync(conf.sslPrivateKey),
        cert: fs.readFileSync(conf.sslCertificate)
    }, app.callback()).listen(conf.apiServicePort);
    debug('use ssl');
}
else {
    require('http').createServer(app.callback()).listen(conf.apiServicePort);
    debug('not use ssl');
}

debug('listening to ' + conf.apiServicePort);
