#!/bin/bash
rabbitmq-plugins enable rabbitmq_management 
service rabbitmq-server start

./rabbitmqadmin import rabbitmq.config
rabbitmqctl add_user $MQ_USER $MQ_PASS
rabbitmqctl set_permissions -p /prod $MQ_USER ".*" ".*" ".*"

service rabbitmq-server restart

service mongod start && \
    service onlinefs-api-service start && \
    service onlinefs-identify-service start && \
    service onlinefs-identify-result-service start && \
    tail -f /var/log/onlinefs-*.log
