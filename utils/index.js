'use strict';

var path = require('path');

require('fs').readdirSync(__dirname).forEach(function (file) {
	if (file === 'index.js' || path.extname(file) != '.js') return;

	var js = path.join(__dirname, file);
	module.exports[path.basename(file, '.js')] = require(js);
});
