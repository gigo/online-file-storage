'use strict';

var conf = require('../config.js')();
var amqp = require('amqp');

var connection = amqp.createConnection(
        { 
            host: conf.amqp.host, 
            port: conf.amqp.port,
            login: conf.amqp.login,
            password: conf.amqp.password,
            connectionTimeout: conf.amqp.connectionTimeout, 
            vhost: conf.amqp.vhost, 
            ssl: { enabled : false}
        },
        { 
            reconnect: true,
            reconnectBackoffStrategy: 'linear',
            reconnectExponentialLimit: 120000,
            reconnectBackoffTime: 3000 
        }
);

exports.connect = () => connection;
