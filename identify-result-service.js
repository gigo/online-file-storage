'use strict';

var debug = require('debug')('identify-result-service');
var error = require('debug')('identify-result-service:err');
var conf = require('./config.js')();
var amqp = require('amqp');
var mongo = require('mongodb');

var connection = require('./utils').mq.connect();

// Wait for connection to become established.
connection.on('ready', function () {
    debug('connection ready');

    let options = {
        passive: true,
        durable: true,
        autoDelete: false,
        arguments: {
            'x-dead-letter-exchange': 'dead.letters'
        }
    };

    connection.queue(conf.amqp.identifyResultQueue, options, queue => {
        // Set ack = true, so that we could have a chance to reject a message, or
        // it will ack automatically
        queue.subscribe({ack: true}, message => {
            if (!message) {
                error(Error('invalid message'));
                queue.shift(true);
                return;
            }

            debug('recv: ' + JSON.stringify(message));

            var url = 'mongodb://' + conf.mongo.host + ':' + conf.mongo.port + '/' + conf.mongo.db;
            mongo.MongoClient.connect(url).then(db => {
                db.collection(conf.mongo.fileCollection).
                findOneAndUpdate(
                        {_id: mongo.ObjectID(message.fileId)}, 
                        {$set: {tags: message.tags}}
                );
                db.close();

                queue.shift();
            })
            .catch(err => {
                error(Error(err));
                queue.shift(true);
            });
        });
    });
});

connection.on('error', function(e) {
    error('err: ' + e);
});
