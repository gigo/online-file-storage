'use strict';


var conf = require('../config.js')();
var jwt = require('jsonwebtoken');

var debug = require('debug')('authentication');
var bcrypt = require('bcryptjs');

exports.register = function *(next) {
    var email = this.request.body.email.toLowerCase();
    var password = this.request.body.password;
    var user = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.authCollection).findOne({'email': email});   

    if (user != null) {
        this.throw(409);
    }

    var salt = bcrypt.genSaltSync(4);
    var hash = bcrypt.hashSync(password, salt);

    yield this.mongo.db(conf.mongo.db).collection(conf.mongo.authCollection).insertOne({
        'email': email,
        'hash': hash,
        'date': (new Date()).getTime(),
    });

    // generate JWT for auth token
    // TODO: the privilege is for future. It may be ['file', 'account', ..]
    // indicate that the token can be used for file/account/.. api access.
    var token = jwt.sign(
            {
                email: email, 
                secret: hash,
                privilege: ['*']
            }, 
            conf.authSecret
    );

    this.body = {token: token, email: email};
}

exports.login = function *(next) {
    var email = this.request.body.email.toLowerCase();
    var password = this.request.body.password;
    var user = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.authCollection).findOne({'email': email});   

    if (user == null) {
        this.throw(403);
    }

    if (!bcrypt.compareSync(password, user.hash)) {
        // The error message/code should be the same as 'account not found', provide 
        // no further info for hacker
        this.throw(403);
    }

    var token = jwt.sign(
            {
                email: user.email, 
                secret: user.hash,
                privilege: ['*']
            }, 
            conf.authSecret
    );

    this.body = {token: token, email: email};
}
