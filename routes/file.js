'use strict';

var debug = require('debug')('file');
var parse = require('co-busboy');
var fs = require('fs');
var os = require('os');
var path = require('path');
var ObjectID = require('mongodb').ObjectID;
var conf = require('../config.js')();
var gcloud = require('gcloud');
var randomstring = require('randomstring');
var amqp = require('amqp');
var connection = require('../utils').mq.connect();
var Promise = require('promise');

exports.preprocess = function *(next) {
    // check privilege
    var privilege = this.state.token.privilege;
    if (privilege.indexOf('*') == -1 && privilege.indexOf('file') == -1 ) {
        this.throw('Permission Denied', 403);
    }

    var email = this.state.token.email;
    var secret = this.state.token.secret;
    var userId = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.authCollection).findOne( {email: email, hash: secret}, {_id: 1});

    if (userId == null) {
        this.throw(403);
    }
    
    this.state.user = userId._id;
    yield next;
}

exports.list = function *(next) {
    // TODO: support paging
    this.body = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.fileCollection).find({userId: this.state.user}, {path:0, userId:0}).toArray();
    yield next
}

function getSignedUrl(remoteFile) {
    return new Promise( (resolve, reject) => {
        remoteFile.getSignedUrl({
            action: 'read',
            expires: '03-17-2025'
        }, 
        (err, url) => {
            if (err) {
                reject(Error(err));
            }

            resolve(url);
        });
    });
}

exports.download = function*(next) {
    var id = this.params.id;
    var result = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.fileCollection).findOne({"_id": ObjectID(id), "userId": this.state.user});

    var gcs = gcloud.storage({
        projectId: conf.gservice.projectId,
        keyFilename: conf.gservice.jsonKey,
    });

    var bucket = gcs.bucket(conf.gservice.storage.bucket);
    var remoteFile = bucket.file(result['path']);

    this.body = remoteFile.createReadStream();
    yield next;
}

exports.getDownloadLink = function *(next) {
    var id = this.params.id;
    var result = yield this.mongo.db(conf.mongo.db).collection(conf.mongo.fileCollection).findOne({"_id": ObjectID(id), "userId": this.state.user});

    var gcs = gcloud.storage({
        projectId: conf.gservice.projectId,
        keyFilename: conf.gservice.jsonKey,
    });

    var bucket = gcs.bucket(conf.gservice.storage.bucket);
    var remoteFile = bucket.file(result['path']);

    var that = this;

    yield new Promise( (resolve, reject) => {
        // FIXME: decide a expire date
        remoteFile.getSignedUrl({
            action: 'read',
            expires: '04-20-2025',
            responseDisposition: 'attachment;filename=' + result['name']
        }, 
        (err, url) => {
            if (err) {
                reject(Error(err));
            }

            that.body = url;
            resolve();
        });
    });

    yield next;
}

exports.upload = function *(next) {
    if ('POST' != this.method) return;

    var gcs = gcloud.storage({
        projectId: conf.gservice.projectId,
        keyFilename: conf.gservice.jsonKey,
    });

    var bucket = gcs.bucket(conf.gservice.storage.bucket);

    // multipart upload
    var parts = parse(this);
    var part;

    var utcMilisecond = (new Date()).getTime();
    while (part = yield parts) {

        // not allow any other parts
        if (part.length) {
            this.throw('Arguments error', 404);
        }

        // to avoid name collision, the bucket path = {user id} / {milisecond}-{random(4)}.{ext}
        var ext = path.extname(part.filename);
        var bucketPath = path.join(this.state.user.toString(), utcMilisecond + "-" + randomstring.generate(4) + ext);
        var remoteFile = bucket.file(bucketPath);

        var remoteStream = remoteFile.createWriteStream();
        part.pipe(remoteStream);
        debug('uploading %s -> %s', part.filename, bucketPath);

        // save record in database
        var _id = new ObjectID();
        this.mongo.db(conf.mongo.db).collection(conf.mongo.fileCollection).insertOne({
            '_id': _id,
            'userId': this.state.user,
            'name': part.filename,
            'path': bucketPath,
            'date': utcMilisecond,
        });

        // trigger automatically identification
        remoteStream.on('finish', () => {
            // FIXME: connection may not yet initialized?
            connection.exchange().publish(
                    conf.amqp.identifyQueue,
                    {fileId: _id, bucketPath: bucketPath},
                    {contentType: 'application/json'}
            );
        });
    }

    this.body = "upload finish";

    yield next;
}

