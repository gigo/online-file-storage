'use strict';

var debug = require('debug')('identify-service');
var error = require('debug')('identify-service:err');
var conf = require('./config.js')();
var amqp = require('amqp');
var gcloud = require('gcloud');
var path = require('path');
var randomstring = require('randomstring');
var fs = require('fs');
var Promise = require('promise');
var mmm = require('mmmagic');
var jimp = require('jimp');

var connection = require('./utils').mq.connect();

// Wait for connection to become established.
connection.on('ready', function () {
    debug('connection ready');

    let options = {
        passive: true,
        durable: true,
        autoDelete: false,
        arguments: {
            'x-dead-letter-exchange': 'dead.letters'
        }
    };

    connection.queue(conf.amqp.identifyQueue, options, queue => {
        // Set ack = true, so that we could have a chance to reject a message, or
        // it will ack automatically
        queue.subscribe({ack: true}, message => {
            if (!message) {
                error(Error('invalid message'));
                queue.shift(true);
                return;
            }

            debug('recv: ' + JSON.stringify(message));
            downloadFromBucket(message)
            .then(checkMimeType)
            .then(resize)
            .then(identifyImage)
            .then(tags => {
                connection.exchange().publish(
                        conf.amqp.identifyResultQueue, 
                        {fileId: message.fileId, tags: tags}, 
                        {contentType: 'application/json'}
                );
                queue.shift();
                debug('file: ' + message.fileId + ' tags: ' + tags);
            })
            .catch(err => {
                error('ERR: ' + err);
                queue.shift(true);
            });
        });
    });
});

connection.on('error', function(e) {
    error('err: ' + e);
});

function downloadFromBucket(msg) {
    return new Promise( (resolve, reject) => {
        var gcs = gcloud.storage({
            projectId: conf.gservice.projectId,
            keyFilename: conf.gservice.jsonKey,
        });

        var cacheFile = path.join('/tmp', randomstring.generate(8));
        var bucket = gcs.bucket(conf.gservice.storage.bucket);

        bucket.file(msg.bucketPath).download({
            destination: cacheFile
        }, err => {
            if (err != null) {
                fs.unlink(cacheFile);
                reject(Error(err));
            }

            resolve(cacheFile);
        });
    });
}

function checkMimeType(file) {
    return new Promise( (resolve, reject) => {
        var magic = new mmm.Magic(mmm.MAGIC_MIME_TYPE);

        magic.detectFile(file, (err, result) => {
            if (err) reject(Error(err));

            if (!result.startsWith('image')) {
                reject(Error('not image type'));
            }

            resolve(file);
        });
    });
}

function resize(file) {
    var write = Promise.denodeify(require('jimp').prototype.write);
    var resizeFile = file + ".rsz.jpg";

    return jimp.read(file)
        .then(image => {
            return image.resize(jimp.AUTO, 250, jimp.RESIZE_BICUBIC);
        })
        .then(image => {
            return write.bind(image)(resizeFile);
        })
        .then( () => {
            fs.unlink(file);
            return resizeFile;
        });    
}

function identifyImage(file) {
    return new Promise( (resolve, reject) => {
        var vision = gcloud.vision({
            projectId: conf.gservice.projectId,
            keyFilename: conf.gservice.jsonKey,
        });

        vision.detect(file, ['label'], (err, detections, apiResonse) => {
            fs.unlink(file);
            resolve(detections);
        });
    });
}
